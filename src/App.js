import React, { useContext } from 'react';
import './App.css';
import {
  Route,
  BrowserRouter as Router,
  Link,
  useRouteMatch,
  Switch,
} from 'react-router-dom';

import Explore from './pages/explore/index'
import Detail from './pages/detail';
import MyLoader from './components/loader';
import { ApplicationContext } from './context';
import NavBar from './components/nav';

function App() {
  const applicationContext = useContext(ApplicationContext)
  const { loading } = applicationContext;

  return (
    <Router>
    <NavBar />
      <div className="App">
        {loading ? <MyLoader />:
          <div>
           
            <Route exact path="/">
              <Explore />
            </Route>
            <Route exact path="/detail/:id">
              <Detail />
            </Route>
          </div>}
      </div>
      
    </Router>
  );
}

export default App;
