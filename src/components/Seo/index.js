import React from "react";
import PropTypes from "prop-types";
import { Helmet } from "react-helmet";

const Seo = ({title="Here is the title"}) => {

    const description = "Here is the description";
    const image = "https://pbs.twimg.com/media/EQeeRYCXsAYdboe.jpg";
    const url ="https://gourban.com";
    const appId='';
    console.log(title);
    return (
        <Helmet>
            {/* General tags */}
            <title>{title}</title>
            <meta name="description" content={description} />
            {/* OpenGraph tags */}
            <meta name="og:url" content={url} />
            <meta name="og:title" content={title} />
            <meta name="og:description" content={description} />
            <meta name="og:image" content={image} />
            <meta name="og:type" content="website" />
            <meta name="fb:app_id" content={appId} />
        </Helmet>
    )
};


export default Seo;