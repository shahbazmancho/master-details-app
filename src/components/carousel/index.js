import React, { useContext } from 'react';
// import { Row, Col } from 'react-bootstrap';
import './style.css'
import MyCarouselItem from './carouse-item';
import { ApplicationContext } from '../../context';
const MyCarousel = () => {

    

    const appContext = useContext(ApplicationContext);

    const { properties,propertiesData } = appContext;
    return (
        <div className="carouselWrapper">
            <div className="carouselTitle">{propertiesData.title}</div>
            <div className="carouselSubtitle">{propertiesData.subtitle}</div>
            <div className="carouselScrollWrapper">
               {properties.length>0?
                properties.map((property) => 
                    <MyCarouselItem property={property} key={property.id}/>
                ):
                <div>
                    Sorry! <br/> No property found for the current query
                </div>}
            </div>
        </div>

    )
}


export default MyCarousel;
