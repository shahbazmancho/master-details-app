/*
 * Filename: /Users/shahbazmancho/Documents/React/master-details-app/src/components/carousel/carouse-item.js
 * Path: /Users/shahbazmancho/Documents/React/master-details-app
 * Created Date: Sunday, March 29th 2020, 9:48:29 pm
 * Author: Shahbaz Mancho
 * 
 * Copyright (c) 2020 Elektrasoft
 */

import React, { useContext } from 'react';
// import property1 from '../../assets/images/property-4.jpg'
import { HeartIcon, BedIcon, BathIcon, TransportIcon } from '../icons';
import { ApplicationContext } from '../../context';
import { useHistory } from "react-router-dom";
const MyCarouselItem = (props) => {
    const history = useHistory();
    const { property } = props;
    const appContext = useContext(ApplicationContext);
    const { wishlist, handleOnAddWishList } = appContext;

    function handleOnClickCarousel(e) {
        const path = '/detail/' + property.id;
        history.push(path)
    }
    function handleOnClickWishlist(e) {
        e.stopPropagation();
        handleOnAddWishList(property.id)
    }
    return (
        <div className="carouselItem" onClick={handleOnClickCarousel}>
            <div className='carouselTop'>
                <img src={property.image} className="carouselImage" alt="Property" />
                <div className="carouselOverlay">
                    <button onClick={handleOnClickWishlist}>
                        <HeartIcon fill={wishlist.includes(String(property.id)) ? 'red' : 'white'} />
                    </button>
                </div>

            </div>
            <div className="carouselBottom">
                <div className="row summaryWrapper">
                    <span>{property.bed}</span>
                    <BedIcon />
                    <span>{property.bath}</span>
                    <BathIcon />
                    <span>{property.parking}</span>
                    <TransportIcon />
                </div>

                <div className="price">
                    {property.price} {property.currency} <span> / {property.price_per}</span>
                </div>
                <div className="address1">
                    {property.address}
                </div>
                <div className="address">
                    {property.builiding}
                </div>
                <div className="tags">
                    {property.tags}
                </div>
            </div>

        </div>
    )
}

export default MyCarouselItem;