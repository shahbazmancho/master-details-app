import React from 'react';
// import { Row, Col } from 'react-bootstrap';
import './style.css'
import { FilterIcon } from '../icons';
function Filters() {
    var filters= [
        {id: 1, label: 'Filters',icon:<FilterIcon/>},
        {id: 2, label: 'Home type',icon:null},
        {id: 3, label: 'Price',icon:null},
        {id: 4, label: 'BRs',icon:null},
        {id: 5, label: 'Amenities',icon:null},
      ]
    return (
        <div className="filterScrollWrapper">
           {filters.map((filter)=>
               <div key={filter.id} className='filter'>
                   {filter.icon}
                   <span>{filter.label}</span>
                </div>
            )}
        </div>
    )
}


export default Filters;
