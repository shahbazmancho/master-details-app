/*
 * Filename: /Users/shahbazmancho/Documents/React/master-details-app/src/components/icons/index.js
 * Path: /Users/shahbazmancho/Documents/React/master-details-app
 * Created Date: Sunday, March 29th 2020, 6:39:10 pm
 * Author: Shahbaz Mancho
 * 
 * Copyright (c) 2020 Elektrasoft
 */

import React from 'react';
import './style.css'

export const SearchIcon = (props) => {
    const { width = 16, height = 17, fill = "white" } = props;
    return (
        <svg className="searchIcon" width={width} height={height} viewBox="0 0 16 17" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M15.7804 15.4003L13.3507 12.9706C14.6888 11.5382 15.4231 9.68133 15.4231 7.7114C15.4231 5.6515 14.621 3.71499 13.1646 2.25854C11.7078 0.802093 9.77129 0 7.7114 0C5.65182 0 3.7153 0.802093 2.25854 2.25854C0.802093 3.7153 0 5.65182 0 7.7114C0 9.77129 0.802093 11.7078 2.25854 13.1643C3.71499 14.6207 5.6515 15.4228 7.7114 15.4228C9.35494 15.4228 10.9198 14.9115 12.225 13.9651L14.7203 16.4604C15.013 16.7531 15.4878 16.7531 15.7804 16.4604C16.0731 16.1677 16.0731 15.693 15.7804 15.4003ZM1.49924 7.7114C1.49924 4.28595 4.28595 1.49924 7.7114 1.49924C11.1368 1.49924 13.9236 4.28595 13.9236 7.7114C13.9236 11.1368 11.1368 13.9236 7.7114 13.9236C4.28595 13.9236 1.49924 11.1368 1.49924 7.7114Z" fill={fill} />
        </svg>
    )
}

export const FilterIcon = (props) => {
    const { width = 16, height = 14, fill = "white" } = props;
    return (
        <svg className="filterIcon" width={width} height={height} viewBox="0 0 16 14" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path fillRule="evenodd" clipRule="evenodd" d="M7.28832 11.5833H1.74999C1.28975 11.5833 0.916656 11.2102 0.916656 10.75C0.916656 10.2898 1.28975 9.91667 1.74999 9.91667H7.28832C7.65695 8.68071 8.79357 7.83362 10.0833 7.83362C11.3731 7.83362 12.5097 8.68071 12.8783 9.91667H14.25C14.7102 9.91667 15.0833 10.2898 15.0833 10.75C15.0833 11.2102 14.7102 11.5833 14.25 11.5833H12.8783C12.5097 12.8193 11.3731 13.6664 10.0833 13.6664C8.79357 13.6664 7.65695 12.8193 7.28832 11.5833ZM11.3333 10.75C11.3333 10.0596 10.7737 9.5 10.0833 9.5C9.39297 9.5 8.83332 10.0596 8.83332 10.75C8.83332 11.4404 9.39297 12 10.0833 12C10.7737 12 11.3333 11.4404 11.3333 10.75Z" fill={fill} />
            <path fillRule="evenodd" clipRule="evenodd" d="M2.70499 4.08333H1.74999C1.28975 4.08333 0.916656 3.71024 0.916656 3.25C0.916656 2.78976 1.28975 2.41667 1.74999 2.41667H2.70499C3.07362 1.18071 4.21024 0.333618 5.49999 0.333618C6.78974 0.333618 7.92636 1.18071 8.29499 2.41667H14.25C14.7102 2.41667 15.0833 2.78976 15.0833 3.25C15.0833 3.71024 14.7102 4.08333 14.25 4.08333H8.29499C7.92636 5.31928 6.78974 6.16638 5.49999 6.16638C4.21024 6.16638 3.07362 5.31928 2.70499 4.08333ZM6.74999 3.25C6.74999 2.55964 6.19035 2 5.49999 2C4.80963 2 4.24999 2.55964 4.24999 3.25C4.24999 3.94036 4.80963 4.5 5.49999 4.5C6.19035 4.5 6.74999 3.94036 6.74999 3.25Z" fill={fill} />
            <defs>
                <linearGradient id="paint0_linear" x1="0.916656" y1="7.83362" x2="2.59917" y2="16.1823" gradientUnits="userSpaceOnUse">
                    <stop stopColor="#F1F1F1" />
                    <stop offset="1" stopColor="#DDDDDD" />
                </linearGradient>
                <linearGradient id="paint1_linear" x1="0.916656" y1="0.333618" x2="2.59917" y2="8.68231" gradientUnits="userSpaceOnUse">
                    <stop stopColor="#F1F1F1" />
                    <stop offset="1" stopColor="#DDDDDD" />
                </linearGradient>
            </defs>
        </svg>

    )
}

export const HeartIcon = (props) => {
    const { width = 18, height = 16, fill = "white", strokeWidth = 1.5,
        backgroundWidth = 32, backgroundHeight = 32,
        backgroundRadius = 36, backgroundColor = 'rgba(53, 62, 77, 0.4)' } = props;
    return (
        <div style={{
            background: backgroundColor, height: backgroundHeight,
            display: 'flex', justifyContent: 'center', alignItems: 'center',
            width: backgroundWidth, borderRadius: backgroundRadius, borderColor: 'rgba(0,0,0,0)'
        }}>
            <svg width={width} height={height} viewBox="0 0 18 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fillRule="evenodd" clipRule="evenodd" d="M16.5743 3.26532C15.9858 2.08452 14.8835 1.26249 13.6069 1.0525C12.3303 0.842513 11.0326 1.26976 10.1136 2.20259L9 3.24489L7.88636 2.20259C6.96711 1.26992 5.66919 0.843022 4.39255 1.05345C3.11591 1.26388 2.01375 2.08638 1.4257 3.2675C0.649177 4.84548 0.954643 6.75715 2.18163 7.99834L7.54533 13.681C8.33473 14.5174 9.66516 14.5173 10.4544 13.6808L15.8184 7.99615C17.0454 6.75496 17.3508 4.84329 16.5743 3.26532Z" stroke={fill} strokeWidth={strokeWidth} strokeLinecap="round" strokeLinejoin="round" />
            </svg>
        </div>
    )

}
export const BedIcon = (props) => {
    const { width = 18, height = 16, fill = "white", strokeWidth = 1.5,
        backgroundWidth = 28, backgroundHeight = 28,
        backgroundRadius = 14, backgroundColor = '#3F3F3F' } = props;
    return (
        <div style={{
            background: backgroundColor, height: backgroundHeight,
            display: 'flex', justifyContent: 'center', alignItems: 'center',
            width: backgroundWidth, borderRadius: backgroundRadius, borderColor: 'rgba(0,0,0,0)'
        }}>
            <svg width={width} height={height} viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fillRule="evenodd" clipRule="evenodd" d="M2.73778 7.28879H13.2622C13.8906 7.28879 14.4 7.81941 14.4 8.47397V11.2888H1.60001V8.47397C1.60001 7.81941 2.10941 7.28879 2.73778 7.28879Z" stroke="white" strokeWidth="0.8" strokeLinecap="round" strokeLinejoin="round" />
                <path d="M1.60001 11.2886V13.0664" stroke="white" strokeWidth="0.8" strokeLinecap="round" strokeLinejoin="round" />
                <path d="M14.3998 11.2886V13.0664" stroke="white" strokeWidth="0.8" strokeLinecap="round" strokeLinejoin="round" />
                <path d="M13.1205 7.28888V3.28888C13.1205 2.79796 12.7385 2.39999 12.2672 2.39999H3.73389C3.2626 2.39999 2.88055 2.79796 2.88055 3.28888V7.28888" stroke="white" strokeWidth="0.8" strokeLinecap="round" strokeLinejoin="round" />
                <path fillRule="evenodd" clipRule="evenodd" d="M6.0084 5.06665H9.99062C10.3048 5.06665 10.5595 5.33196 10.5595 5.65924V7.28887H5.43951V5.65924C5.43951 5.33196 5.69421 5.06665 6.0084 5.06665Z" stroke="white" strokeWidth="0.8" strokeLinecap="round" strokeLinejoin="round" />
            </svg>
        </div>
    )

}
export const BathIcon = (props) => {
    const { width = 16, height = 16, fill = "white", strokeWidth = 1.5,
        backgroundWidth = 28, backgroundHeight = 28,
        backgroundRadius = 14, backgroundColor = '#3F3F3F' } = props;
    return (
        <div style={{
            background: backgroundColor, height: backgroundHeight,
            display: 'flex', justifyContent: 'center', alignItems: 'center',
            width: backgroundWidth, borderRadius: backgroundRadius, borderColor: 'rgba(0,0,0,0)'
        }}>
            <svg width={width} height={height} viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M8.46655 9.39996H14.9998L14.5866 11.8801C14.2867 13.6805 12.7289 15 10.9038 14.9999H5.09602C3.27082 15 1.71309 13.6805 1.41315 11.8801L1 9.39996H3.79995" stroke={fill} strokeWidth="0.8" strokeLinecap="round" strokeLinejoin="round" />
                <path fillRule="evenodd" clipRule="evenodd" d="M8.00007 13.133H4.2668C4.00907 13.133 3.80014 12.9241 3.80014 12.6664V9.39975C3.80014 8.88429 4.218 8.46643 4.73346 8.46643H7.53341C8.04887 8.46643 8.46673 8.88429 8.46673 9.39975V12.6664C8.46673 12.9241 8.2578 13.133 8.00007 13.133Z" stroke={fill} strokeWidth="0.8" strokeLinecap="round" strokeLinejoin="round" />
                <path d="M10.3335 2.86664C10.3335 1.83572 11.1692 1 12.2001 1C13.231 1 14.0667 1.83572 14.0667 2.86664V9.39987" stroke={fill} strokeWidth="0.8" strokeLinecap="round" strokeLinejoin="round" />
                <path fillRule="evenodd" clipRule="evenodd" d="M8.46558 4.73325C8.46558 3.70233 9.3013 2.86661 10.3322 2.86661C11.3631 2.86661 12.1988 3.70233 12.1988 4.73325H8.46558Z" stroke={fill} strokeWidth="0.8" strokeLinecap="round" strokeLinejoin="round" />
            </svg>

        </div>
    )

}
export const TransportIcon = (props) => {
    const { width = 16, height = 16, fill = "white", strokeWidth = 1.5,
        backgroundWidth = 28, backgroundHeight = 28,
        backgroundRadius = 14, backgroundColor = '#3F3F3F' } = props;
    return (
        <div style={{
            background: backgroundColor, height: backgroundHeight,
            display: 'flex', justifyContent: 'center', alignItems: 'center',
            width: backgroundWidth, borderRadius: backgroundRadius, borderColor: 'rgba(0,0,0,0)'
        }}>
            <svg width={width} height={height} viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fillRule="evenodd" clipRule="evenodd" d="M12.4783 12.5C13.0306 12.5 13.4783 12.0523 13.4783 11.5C13.4783 10.9477 13.0306 10.5 12.4783 10.5C11.926 10.5 11.4783 10.9477 11.4783 11.5C11.4783 12.0523 11.926 12.5 12.4783 12.5Z" stroke={fill} strokeWidth="0.8" strokeLinecap="round" strokeLinejoin="round" />
                <path fillRule="evenodd" clipRule="evenodd" d="M3.47832 12.5C4.0306 12.5 4.47832 12.0523 4.47832 11.5C4.47832 10.9477 4.0306 10.5 3.47832 10.5C2.92603 10.5 2.47832 10.9477 2.47832 11.5C2.47832 12.0523 2.92603 12.5 3.47832 12.5Z" stroke={fill} strokeWidth="0.8" strokeLinecap="round" strokeLinejoin="round" />
                <path d="M13.4785 11.5H14.4999C15.0522 11.5 15.4999 11.0523 15.4999 10.5C15.49 8.83894 14.1396 7.49814 12.4785 7.5C12.4785 7.5 10.9785 3 6.97853 3C3.97853 3 0.755867 4.788 0.500534 10.4773C0.488903 10.7442 0.586719 11.0043 0.771345 11.1973C0.955971 11.3904 1.2114 11.4997 1.47853 11.5H2.47853" stroke={fill} strokeWidth="0.8" strokeLinecap="round" strokeLinejoin="round" />
                <path d="M11.4783 11.5H4.47832" stroke={fill} strokeWidth="0.8" strokeLinecap="round" strokeLinejoin="round" />
                <path d="M12.4784 7.5H0.989059" stroke={fill} strokeWidth="0.8" strokeLinecap="round" strokeLinejoin="round" />
                <path d="M6.5 7.5V3.01666" stroke={fill} strokeWidth="0.8" strokeLinecap="round" strokeLinejoin="round" />
            </svg>


        </div>
    )

}

export const ExploreIcon = (props) => {
    let { fill = "#5A5A5A", active = false } = props;
    if (active) {
        fill = "white";
    }

    return (<svg width="29" height="28" viewBox="0 0 29 28" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path fillRule="evenodd" clipRule="evenodd" d="M18.1778 23.8246C23.6045 21.5186 26.1341 15.2506 23.8278 9.82463C21.5215 4.39864 15.2527 1.86938 9.82594 4.17537C4.39922 6.48136 1.86963 12.7494 4.17593 18.1754C6.48223 23.6014 12.7511 26.1306 18.1778 23.8246Z" stroke={fill} strokeWidth="2.4" strokeLinecap="round" strokeLinejoin="round" />
        <path fillRule="evenodd" clipRule="evenodd" d="M21.5506 21.5477L25.67 25.6667L21.5506 21.5477Z" fill="#5353DB" />
        <path d="M21.5506 21.5477L25.67 25.6667" stroke={fill} strokeWidth="2.4" strokeLinecap="round" strokeLinejoin="round" />
    </svg>

    )
}

export const SavedIcon = (props) => {
    let { fill = "#5A5A5A", active = false } = props;
    if (active) {
        fill = "white";
    }

    return (<svg width="30" height="26" viewBox="0 0 30 26" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path fillRule="evenodd" clipRule="evenodd" d="M27.3082 5.82272C26.352 3.83013 24.5607 2.44296 22.4862 2.0886C20.4117 1.73424 18.303 2.45521 16.8097 4.02938L15 5.78826L13.1903 4.02938C11.6966 2.45549 9.58744 1.7351 7.5129 2.0902C5.43836 2.4453 3.64734 3.83327 2.69176 5.82641C1.42991 8.48924 1.92629 11.7152 3.92015 13.8097L12.0395 22.7428C13.6268 24.4891 16.373 24.4889 17.96 22.7424L26.0798 13.806C28.0737 11.7115 28.5701 8.48555 27.3082 5.82272Z" stroke={fill} strokeWidth="2.4" strokeLinecap="round" strokeLinejoin="round" />
    </svg>
    )

}
export const HomeIcon = (props) => {
    let { fill = "#5A5A5A", active = false } = props;
    if (active) {
        fill = "white";
    }
    return (<svg width="28" height="27" viewBox="0 0 28 27" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path fillRule="evenodd" clipRule="evenodd" d="M24.9568 25C25.5322 24.9982 25.9982 24.5322 26 23.9568V10.6C25.9815 10.2742 25.8374 9.96829 25.5979 9.74667L16.4085 2.8164C14.9828 1.74121 13.0172 1.74121 11.5915 2.8164L2.40213 9.74667C2.16263 9.96829 2.01846 10.2742 2 10.6V23.9568C2.00176 24.5322 2.46779 24.9982 3.0432 25H24.9568Z" stroke={fill} strokeWidth="2.4" strokeLinecap="round" strokeLinejoin="round" />
    </svg>

    )
}

export const ProfileIcon = (props) => {
    let { fill = "#5A5A5A", active = false } = props;
    if (active) {
        fill = "white";
    }

    return (<svg width="23" height="28" viewBox="0 0 23 28" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path fillRule="evenodd" clipRule="evenodd" d="M11.7361 12.4851C14.6315 12.4851 16.9787 10.1379 16.9787 7.24255C16.9787 4.34717 14.6315 2 11.7361 2C8.84075 2 6.49358 4.34717 6.49358 7.24255C6.49358 10.1379 8.84075 12.4851 11.7361 12.4851Z" stroke={fill} strokeWidth="2.4" strokeLinecap="round" strokeLinejoin="round" />
        <path d="M2 26C2 20.6228 6.35903 17.2 11.7362 17.2C17.1133 17.2 21.4723 20.6228 21.4723 26" stroke={fill} strokeWidth="2.4" strokeLinecap="round" strokeLinejoin="round" />
    </svg>

    )
}


export const BackIcon = (props) => {
    const { width = 18, height = 16, fill = "white", strokeWidth = 1.5,
        backgroundWidth = 32, backgroundHeight = 32,
        backgroundRadius = 16, backgroundColor = 'rgba(53, 62, 77, 0.4)' } = props;
    return (
        <div style={{
            background: backgroundColor, height: backgroundHeight,
            display: 'flex', justifyContent: 'center', alignItems: 'center',
            width: backgroundWidth, borderRadius: backgroundRadius, borderColor: 'rgba(0,0,0,0)'
        }}>
            <svg width="8" height="14" viewBox="0 0 8 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                <mask id="path-1-outside-1" maskUnits="userSpaceOnUse" x="0" y="0" width={backgroundWidth} height={backgroundHeight} fill={backgroundColor}>
                    <rect fill="white" width="8" height="14" />
                    <path fillRule="evenodd" clipRule="evenodd" d="M6.80624 1.21774C6.54793 0.927421 6.12913 0.927422 5.87082 1.21774L1.19373 6.47434C0.935423 6.76465 0.935423 7.23534 1.19373 7.52566C1.19541 7.52754 1.1971 7.52942 1.19879 7.53128L5.87089 12.7823C6.1292 13.0726 6.548 13.0726 6.80631 12.7823C7.06461 12.492 7.06461 12.0213 6.80631 11.7309L2.59689 6.99997L6.80624 2.26906C7.06455 1.97874 7.06455 1.50805 6.80624 1.21774Z" />
                </mask>
                <path fillRule="evenodd" clipRule="evenodd" d="M6.80624 1.21774C6.54793 0.927421 6.12913 0.927422 5.87082 1.21774L1.19373 6.47434C0.935423 6.76465 0.935423 7.23534 1.19373 7.52566C1.19541 7.52754 1.1971 7.52942 1.19879 7.53128L5.87089 12.7823C6.1292 13.0726 6.548 13.0726 6.80631 12.7823C7.06461 12.492 7.06461 12.0213 6.80631 11.7309L2.59689 6.99997L6.80624 2.26906C7.06455 1.97874 7.06455 1.50805 6.80624 1.21774Z" fill="white" />
                <path d="M5.87082 1.21774L5.27315 0.685955L5.27315 0.685955L5.87082 1.21774ZM6.80624 1.21774L7.40391 0.685955L7.40391 0.685955L6.80624 1.21774ZM1.19373 6.47434L0.596062 5.94256H0.596062L1.19373 6.47434ZM1.19373 7.52566L1.79141 6.99388L1.7914 6.99388L1.19373 7.52566ZM1.19879 7.53128L1.79649 6.99947L1.79082 6.99323L1.19879 7.53128ZM5.87089 12.7823L6.46856 12.2505L6.46856 12.2505L5.87089 12.7823ZM6.80631 12.7823L6.20864 12.2505V12.2505L6.80631 12.7823ZM6.80631 11.7309L7.40398 11.1992L7.40398 11.1992L6.80631 11.7309ZM2.59689 6.99997L1.99922 6.46819L1.52606 6.99997L1.99922 7.53175L2.59689 6.99997ZM6.80624 2.26906L6.20857 1.73728V1.73728L6.80624 2.26906ZM6.46849 1.74952C6.45038 1.76988 6.40437 1.8 6.33853 1.8C6.2727 1.8 6.22669 1.76988 6.20857 1.74952L7.40391 0.685955C6.8274 0.0380146 5.84966 0.0380154 5.27315 0.685955L6.46849 1.74952ZM1.7914 7.00612L6.46849 1.74952L5.27315 0.685955L0.596062 5.94256L1.7914 7.00612ZM1.7914 6.99388C1.79848 7.00184 1.80074 7.00763 1.80095 7.00822C1.80101 7.00838 1.8 7.00533 1.8 7C1.8 6.99467 1.80101 6.99161 1.80095 6.99177C1.80074 6.99236 1.79848 6.99816 1.7914 7.00612L0.596062 5.94256C0.0679799 6.53607 0.067979 7.46392 0.596062 8.05744L1.7914 6.99388ZM1.79082 6.99323L1.79141 6.99388L0.596054 8.05743C0.599556 8.06136 0.603123 8.06533 0.606755 8.06933L1.79082 6.99323ZM6.46856 12.2505L1.79646 6.9995L0.601117 8.06306L5.27322 13.314L6.46856 12.2505ZM6.20864 12.2505C6.22675 12.2301 6.27276 12.2 6.3386 12.2C6.40443 12.2 6.45044 12.2301 6.46856 12.2505L5.27322 13.314C5.84973 13.962 6.82747 13.962 7.40398 13.314L6.20864 12.2505ZM6.20864 12.2627C6.20155 12.2548 6.1993 12.249 6.19909 12.2484C6.19903 12.2482 6.20004 12.2513 6.20004 12.2566C6.20004 12.2619 6.19903 12.265 6.19909 12.2648C6.1993 12.2642 6.20155 12.2584 6.20864 12.2505L7.40398 13.314C7.93206 12.7205 7.93206 11.7927 7.40398 11.1992L6.20864 12.2627ZM1.99922 7.53175L6.20864 12.2627L7.40398 11.1992L3.19456 6.46819L1.99922 7.53175ZM6.20857 1.73728L1.99922 6.46819L3.19456 7.53175L7.40391 2.80084L6.20857 1.73728ZM6.20857 1.74952C6.20149 1.74156 6.19924 1.73576 6.19903 1.73517C6.19897 1.73501 6.19997 1.73807 6.19997 1.7434C6.19997 1.74872 6.19897 1.75178 6.19903 1.75162C6.19924 1.75103 6.20149 1.74524 6.20857 1.73728L7.40391 2.80084C7.93199 2.20732 7.93199 1.27947 7.40391 0.685955L6.20857 1.74952Z" fill="white" mask="url(#path-1-outside-1)" />
            </svg>
        </div>
    )

}


export const CameraIcon = () => {
    return (<svg width="32" height="23" viewBox="0 0 32 23" fill="none" xmlns="http://www.w3.org/2000/svg">
        <ellipse cx="16" cy="17" rx="16" ry="6" fill="#5A5A5A" />
        <ellipse cx="16" cy="17" rx="10" ry="3" fill="#ADADAD" />
        <circle cx="16" cy="8.75" r="6.25" fill="#232323" />
        <path d="M16 11.539C17.6993 11.539 19.0769 10.1614 19.0769 8.46206C19.0769 6.76272 17.6993 5.38513 16 5.38513C14.3007 5.38513 12.9231 6.76272 12.9231 8.46206C12.9231 10.1614 14.3007 11.539 16 11.539Z" fill="#F1F1F1" />
        <path d="M26 3.84664C26 3.2346 25.7569 2.64763 25.3241 2.21485C24.8913 1.78208 24.3043 1.53894 23.6923 1.53894H20.1692C20.1004 1.53917 20.0328 1.52086 19.9735 1.48594C19.9143 1.45102 19.8655 1.40077 19.8323 1.34048C19.656 0.934945 19.3631 0.591002 18.9908 0.352392C18.6185 0.113781 18.1836 -0.00871445 17.7415 0.000482363H14.2585C13.8164 -0.00871445 13.3815 0.113781 13.0092 0.352392C12.6369 0.591002 12.344 0.934945 12.1677 1.34048C12.1345 1.40077 12.0857 1.45102 12.0265 1.48594C11.9672 1.52086 11.8996 1.53917 11.8308 1.53894H8.30769C7.69565 1.53894 7.10868 1.78208 6.67591 2.21485C6.24313 2.64763 6 3.2346 6 3.84664V13.0774C6 13.6894 6.24313 14.2764 6.67591 14.7092C7.10868 15.142 7.69565 15.3851 8.30769 15.3851H23.6923C24.3043 15.3851 24.8913 15.142 25.3241 14.7092C25.7569 14.2764 26 13.6894 26 13.0774V3.84664ZM16 3.84664C16.9128 3.84664 17.8052 4.11732 18.5642 4.62447C19.3232 5.13161 19.9147 5.85244 20.2641 6.69579C20.6134 7.53914 20.7048 8.46714 20.5267 9.36244C20.3486 10.2577 19.909 11.0801 19.2636 11.7256C18.6181 12.3711 17.7957 12.8106 16.9004 12.9887C16.0051 13.1668 15.0771 13.0754 14.2338 12.7261C13.3904 12.3768 12.6696 11.7852 12.1624 11.0262C11.6553 10.2672 11.3846 9.37486 11.3846 8.46202C11.3846 7.23795 11.8709 6.064 12.7364 5.19845C13.602 4.3329 14.7759 3.84664 16 3.84664Z" fill="#F1F1F1" />
    </svg>
    )
}
export const TourIcon = (props) => {

    return (
        <div className="TourIcon">
            <CameraIcon /> <span>Virtual Tour</span>
        </div>
    )
}

export const GalleryIcon = () => {
    return (
        <div className="GalleryIcon">
            <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M26.6312 13.169C26.7327 12.6248 26.6142 12.0626 26.3018 11.6056C25.9893 11.1486 25.5085 10.8342 24.9646 10.7312L20.5201 9.90007C20.4801 9.89255 20.439 9.89392 20.3996 9.9041C20.3603 9.91428 20.3236 9.93301 20.2923 9.95896C20.261 9.98502 20.2357 10.0176 20.2182 10.0545C20.2007 10.0913 20.1915 10.1315 20.1912 10.1723V11.8679C20.1912 11.9329 20.214 11.9958 20.2556 12.0458C20.2972 12.0957 20.355 12.1295 20.419 12.1412L23.8757 12.7879C24.0205 12.8148 24.1487 12.8982 24.232 13.0197C24.3154 13.1412 24.3471 13.2908 24.3201 13.4356L21.8679 26.5468C21.8409 26.6916 21.7575 26.8198 21.636 26.9032C21.5146 26.9865 21.3649 27.0182 21.2201 26.9913L8.19783 24.5557C8.053 24.5287 7.92481 24.4453 7.84146 24.3238C7.75812 24.2024 7.72643 24.0528 7.75338 23.9079L7.87116 23.279C7.87963 23.2385 7.87892 23.1967 7.86908 23.1565C7.85924 23.1163 7.84052 23.0789 7.8143 23.0469C7.78807 23.0149 7.75501 22.9892 7.71755 22.9717C7.68009 22.9542 7.63918 22.9453 7.59782 22.9457H5.90226C5.83803 22.9454 5.7757 22.9674 5.72586 23.0079C5.67603 23.0484 5.64177 23.105 5.62892 23.1679L5.44226 24.1679C5.33948 24.713 5.45728 25.2767 5.76978 25.735C6.08228 26.1934 6.56393 26.5089 7.10893 26.6124L21.4923 29.3024C21.6183 29.3262 21.7463 29.3381 21.8746 29.3379C22.362 29.3364 22.8337 29.1649 23.2084 28.853C23.583 28.5411 23.8371 28.1082 23.9268 27.629L26.6312 13.169Z" fill="white" />
                <rect x="1.33333" y="4" width="16" height="14.6667" fill="white" fillOpacity="0.56" />
                <path d="M7 15C7 10.5818 10.5817 7.00003 15 7.00003V15H7Z" fill="white" fillOpacity="0.56" />
                <path d="M16.7179 21.5556C17.2717 21.5542 17.8023 21.333 18.1933 20.9408C18.5843 20.5486 18.8037 20.0172 18.8034 19.4634V4.75778C18.8037 4.20405 18.5843 3.67283 18.1932 3.28077C17.8022 2.8887 17.2716 2.66783 16.7179 2.66666L2.08668 2.66666C1.53285 2.66783 1.0021 2.88867 0.610903 3.2807C0.219703 3.67273 -9.52777e-07 4.20395 2.98139e-07 4.75778V19.4634C-0.000295333 20.0174 0.219278 20.5489 0.610511 20.9412C1.00174 21.3335 1.53265 21.5545 2.08668 21.5556H16.7179ZM16.0256 4.88889C16.173 4.88889 16.3143 4.94742 16.4185 5.05161C16.5227 5.1558 16.5812 5.2971 16.5812 5.44445V16.4901C16.5813 16.5266 16.5743 16.5628 16.5603 16.5965C16.5464 16.6303 16.526 16.661 16.5002 16.6868C16.4743 16.7126 16.4437 16.7331 16.4099 16.747C16.3761 16.7609 16.3399 16.768 16.3034 16.7678H2.50001C2.42634 16.7678 2.35569 16.7386 2.30359 16.6865C2.2515 16.6344 2.22223 16.5637 2.22223 16.4901V5.44445C2.22223 5.2971 2.28076 5.1558 2.38495 5.05161C2.48914 4.94742 2.63045 4.88889 2.77779 4.88889H16.0256Z" fill="white" />
            </svg>
        </div>
    )
}


// export default SearchIcon;