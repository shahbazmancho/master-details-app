import React, { useContext } from 'react';
// import { Row, Col } from 'react-bootstrap';
import './style.css'
import { SearchIcon } from '../icons';
import { ApplicationContext } from '../../context';
function SearchBar() {

    const appContext=useContext(ApplicationContext);

    const {handleSearchChange}=appContext;


    return (
        <div className='searchBar'>
            <SearchIcon/>
            <label className="searchLabel">Search</label>
            <input className="searchInput" placeholder="Neighborhood, building..."
            onChange={(e)=>handleSearchChange(e)}
            ></input>
            
        </div>
    );
}


export default SearchBar;
