/*
 * Filename: /Users/shahbazmancho/Documents/React/master-details-app/src/components/loader/index.js
 * Path: /Users/shahbazmancho/Documents/React/master-details-app
 * Created Date: Monday, March 30th 2020, 3:41:51 pm
 * Author: Shahbaz Mancho
 * 
 * Copyright (c) 2020 Elektrasoft
 */

import React from 'react';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import Loader from 'react-loader-spinner';
import './style.css'
function MyLoader() {
    return (
        <div className='Loading-header'>
           <Loader
            type="Triangle"
            color="#ffffff"
            height={200}
            width={200}
            // timeout={3500} //3.5 secs
        /> 
        <p className="Loading-text">Loading</p>
        </div>
    );
}

export default MyLoader;
