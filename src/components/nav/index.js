import React, { useContext } from 'react';
// import { Row, Col } from 'react-bootstrap';
import './style.css'
import { Link, useLocation } from 'react-router-dom';
import { ExploreIcon, SavedIcon, HomeIcon, ProfileIcon } from '../icons';
import { ApplicationContext } from '../../context';
import { useToasts } from 'react-toast-notifications';
function NavBar() {
    const applicationContext = useContext(ApplicationContext)
    const { handleLogin } = applicationContext;
    const { addToast } = useToasts()
    let location = useLocation();
    function underDevelopment() {
        addToast('Page is under development', {
            appearance: 'warning',
            autoDismiss: true,
          });
    }
    
    if(location.pathname.includes('/detail')){
        return(<div></div>)
    }
    return (
        <div className='navBarContainer'>
            <nav>
                <Link to="/">
                    <div className="link"><ExploreIcon /> <span> Explore </span></div>
                </Link>
                <Link to="/" onClick={underDevelopment}>
                    <div className="link"><SavedIcon /> <span> Saved </span></div>
                </Link>
                <Link to="/" onClick={underDevelopment}>
                    <div className="link"><HomeIcon /> <span> My Home </span></div>
                </Link>
                <Link to="/" onClick={handleLogin}>
                    <div className="link" ><ProfileIcon /> <span> Profile </span></div>
                </Link>
            </nav>
        </div>
    );
}


export default NavBar;
