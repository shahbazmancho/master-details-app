/*
 * Created Date: Sunday, March 29th 2020, 2:56:02 pm
 * Author: Shahbaz Mancho
 * 
 * Copyright (c) 2020 Elektrasoft
 */

 

 

import React from 'react';
import SearchBar from '../../components/search-bar';
import Filters from '../../components/filters';
import MyCarousel from '../../components/carousel';
import Seo from '../../components/Seo';

const Explore = () => {
    return (
        <div className="exploreContainer">
            <Seo title="Explore Page"/>
            {/* {loading?<MyLoader/>:null} */}
            <SearchBar />
            <Filters />
            <div>
                <h1>Explore</h1>
            </div>
            <MyCarousel />
            {/* <MyCarousel /> */}
            {/* <Footer /> */}

        </div>
    )
}

export default Explore;