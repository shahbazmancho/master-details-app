const colors = {
    background: '#E5E5E5',
    /* Charcoal / 100 */
    charcoal100:'#232323',
    charcoal80:'#3F3F3F',
  }
  
  export default colors