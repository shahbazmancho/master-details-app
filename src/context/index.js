/*
 * Filename: /Users/shahbazmancho/Documents/React/master-details-app/src/context/AppContext.js
 * Path: /Users/shahbazmancho/Documents/React/master-details-app
 * Created Date: Monday, March 30th 2020, 2:11:28 pm
 * Author: Shahbaz Mancho
 * 
 * Copyright (c) 2020 Elektrasoft
 */
import React, { useState, useEffect } from 'react';
import Axios from 'axios';
import { useToasts } from 'react-toast-notifications'
const ApplicationContext = React.createContext();


const ApplicationProvider = (props) => {
    const { addToast } = useToasts()
    const baseUrl="https://www.biphotographers.com/";
    let propertiesUrl = baseUrl+"api/urban/get-properties";
   
    const [properties, setProperties] = useState([]);
    const [propertiesData, setPropertiesData] = useState([]);
    const [wishlist, setWishlist] = useState([]);
    const [isLogin, setIsLogin] = useState(false);
    const [loading, setLoading] = useState(true);
    const [search, setSearch] = useState('');

    useEffect(() => {
        fetchProperties();
    }, [propertiesUrl])
    const fetchProperties = async () => {
        const proxyurl = "https://cors-anywhere.herokuapp.com/";
        propertiesUrl = proxyurl + propertiesUrl;
        await Axios .get(propertiesUrl)
        // Once we get a response, we'll map the API endpoints to our props
        .then(response =>{
            const data=response.data;
            setPropertiesData(data)
            setProperties(data.properties)
            setLoading(false)
        })
        // We can still use the `.catch()` method since axios is promise-based
        .catch(error =>  setLoading(false));
    }
    const fetchUserWishList = async () => {
        setLoading(true);
        let url = baseUrl+"api/urban/get-wishlists";
        const proxyurl = "https://cors-anywhere.herokuapp.com/";
        url = proxyurl + url;
        await Axios .get(url)
        .then(response =>{
            const data=response.data;
            setWishlist(data);
            setLoading(false);
        })
        .catch(error =>   setLoading(false));
    }
    const userUpdateWishList = async (id) => {
        setLoading(true);
        let url = baseUrl+"api/urban/update-wishlist/"+id;
        const proxyurl = "https://cors-anywhere.herokuapp.com/";
        url = proxyurl + url;
        await Axios .get(url)
        .then(response =>{
            const data=response.data;
            //console.log(data);
            setWishlist(data)
            setLoading(false);
            addToast('Wishlist updated', {
                appearance: 'success',
                autoDismiss: true,
              });
        })
        .catch(error =>   setLoading(false));
    }

    

    const handleLogin=async()=>{
        if(isLogin==true){
            //do logout and clear wishlist
            setIsLogin(false);
            setWishlist([]);
            addToast('User logged out', {
                appearance: 'success',
                autoDismiss: true,
              });
        }
        else{
              //do login and fetch user wishlist
            setIsLogin(true);
            fetchUserWishList();
            addToast('User logged in', {
                appearance: 'success',
                autoDismiss: true,
              });
        }
    }
    const handleOnAddWishList=async(id)=>{
        id=String(id);
        if(isLogin==true){
            userUpdateWishList(id);
        }
        else{
            if(wishlist.includes(id)){
                
                var index = wishlist.indexOf(id);
                if (index !== -1) wishlist.splice(index, 1);
            }
            else{
                wishlist.push(id);
            }
            setWishlist([...wishlist]);
           
            addToast('Wishlist updated', {
                appearance: 'success',
                autoDismiss: true,
              });
        }
        
        
    }

    //onChangeSearch
    const handleSearchChange = (e) => {
        let query = e.target.value.trim();
        query = query.toLowerCase();
        setSearch(query);
        if (query === "") {
            setProperties(propertiesData.properties);
            return;
        }
        const properties = propertiesData.properties.filter(property => {
            const address = property.address.toLowerCase();
            const builiding = property.builiding.toLowerCase();

            return address.includes(query) || builiding.includes(query);
        });
        setProperties(properties);
    }
    return (
        <ApplicationContext.Provider value={{
            loading,
            search,
            propertiesData,
            properties,
            wishlist,
            handleSearchChange,
            handleLogin,
            handleOnAddWishList
        }}>
            {props.children}
        </ApplicationContext.Provider>
    )

}

const ApplicationConsumer = ApplicationContext.Consumer
export { ApplicationProvider, ApplicationConsumer, ApplicationContext };

